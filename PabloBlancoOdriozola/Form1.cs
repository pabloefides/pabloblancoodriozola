﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EstructuraCondicionalAnidada1
{
    class Program
    {
        static void Main(string[] args)
        {
            int nota1, nota2, nota3;
            string linea;
            Console.Write("Ingrese la  primera nota:");
            linea = Console.ReadLine();
            nota1 = int.Parse(linea);
            Console.Write("Ingrese la segunda nota:");
            linea = Console.ReadLine();
            nota2 = int.Parse(linea);
            Console.Write("Ingrese la tercera nota:");
            linea = Console.ReadLine();
            nota3 = int.Parse(linea);
            int promedio = (nota1 + nota2 + nota3) / 3;
            if (promedio < 5)
            {
                Console.Write("NO APTO");
            }
            else
            {
                if (promedio >= 5 && promedio < 9)
                {
                    Console.Write("APTO");
                }
                else
                {
                    Console.Write("SOBRESALIENTE");
                }
            }
            Console.ReadKey();
        }
    }
}